package com.jsp.appservice.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="app_service")
public class AppService implements Serializable{
	
	@Id
	@GenericGenerator(name="auto-reg",strategy="increment")
	@GeneratedValue(generator="auto-reg")
	@Column(name="alt_key")
	private int altKey;
	
	
	@Column(name="service_name")
	private String serviceName;
	
	@Column(name="service_url")
	private String serviceUrl;
	
	@Column(name="status")
	private String status;
	
	@Column(name="created_date")
	private Date createdDate;
	
	@Column(name="modified_date")
	private Date modifiedDate;

	public int getAltKey() {
		return altKey;
	}

	public void setAltKey(int altKey) {
		this.altKey = altKey;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceUrl() {
		return serviceUrl;
	}

	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Override
	public String toString() {
		return "AppService [altKey=" + altKey + ", serviceName=" + serviceName + ", serviceUrl=" + serviceUrl
				+ ", status=" + status + ", createdDate=" + createdDate + ", modifiedDate=" + modifiedDate + "]";
	}
	

}
