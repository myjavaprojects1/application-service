package com.jsp.appservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class MySqlConfig {

	@Bean
	public HikariDataSource getDataSource(){
		
		String password = System.getenv("MYSQL_PWD");
		String userName = System.getenv("MYSQL_USERNAME");
		String portNumber = System.getenv("PORT_NUMBER");
		
//		System.out.println(System.getenv("OS"));
//		System.out.println(System.getenv("PATH"));
		
		System.out.println(password);
		System.out.println(userName);
		System.out.println(portNumber);
		
		HikariConfig config = new HikariConfig();
		config.setUsername("root");
		config.setPassword("root");
		config.setJdbcUrl("jdbc:mysql://localhost:3306/operational_db");
		config.setDriverClassName("com.mysql.cj.jdbc.Driver");
		return new HikariDataSource(config);
	}
	
	@Bean
	public JdbcTemplate getJdbcTemplate(HikariDataSource hikariDataSource) {
		return new JdbcTemplate(hikariDataSource);
		 
	}
}
